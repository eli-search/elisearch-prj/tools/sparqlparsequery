package com.example.springboot;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import org.apache.jena.graph.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.OpVisitorBase;
import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.graph.GraphFactory;
import org.apache.jena.rdf.model.RDFNode;


class FederatedQueryAlgebraVisitor extends OpVisitorBase {
    // helper class to represent query objects (see https://citnet.tech.ec.europa.eu/CITnet/confluence/display/ELISEARCH/v2+-+Federation+Service)

    private class QueryObject {
        private RDFNode searchTerm;
        private RDFNode language;

        RDFNode getSearchTerm() {
            return searchTerm;
        }

        void setSearchTerm(RDFNode searchTerm) {
            this.searchTerm = searchTerm;
        }

        RDFNode getLanguage() {
            return language;
        }

        void setLanguage(RDFNode language) {
            this.language = language;
        }
    }

    // SPARQL query for retrieving the op-fts:field parameters from  query graph
    private static final String getFieldsQuery =
            "PREFIX op-fts: <http://publications.europa.eu/elisearch/fts#>\n" +
            "SELECT ?field WHERE { op-fts:config op-fts:field ?field. }";

    // SPARQL query for retrieving op-fts:return variable from query graph
    private static final String getReturnVariable =
            "PREFIX op-fts: <http://publications.europa.eu/elisearch/fts#>\n" +
                    "SELECT ?returnVariable WHERE { op-fts:config op-fts:return ?returnVariable. }";

    // list of RDFNodes representing the search fields
    private final List<RDFNode> searchFields = new ArrayList<RDFNode>();
    // list of query object
    private final List<QueryObject> queryObjects = new ArrayList<QueryObject>();
    // RDFNode representing op-fts:return
    private RDFNode returnVariable;


    // fix triples extracted from SPARQL algebra so we can use them to generate the query graph
    Triple fixTriple(Triple triple) {
        // convert subject (??0 to <__0>)
        Node s = triple.getSubject().isVariable() ?
                NodeFactory.createURI(triple.getSubject().toString().replace("?", "_")) :
                triple.getSubject();

        // nothing to be done for predicates
        Node p = triple.getPredicate();

        // convert object
        Node o = triple.getObject().isVariable() ?
                NodeFactory.createURI(triple.getObject().toString().replace("?", "_")) :
                (triple.getObject().isLiteral() ?
                        NodeFactory.createLiteral(triple.getObject().toString().replace("\"", "")) :
                        triple.getObject());

        return Triple.create(s, p, o);
    }

    @Override
    public void visit(OpBGP opBGP) {
        // create new graph
        Graph graph = GraphFactory.createDefaultGraph();

        // extract triples from SPARQL BGP and add them to graph
        List<Triple> tripleList = opBGP.getPattern().getList();
        // fix nodes starting with ? or ??
        for (Triple triple : tripleList) {
            Triple fixedTriple = fixTriple(triple);
            graph.add(fixedTriple);
        }

        // create new RDF model from graph
        Model model = ModelFactory.createModelForGraph(graph);
        //model.write(System.out, "TURTLE");

        // extract op-fts:field parameters
        Query fieldsQquery = QueryFactory.create(this.getFieldsQuery);
        try (QueryExecution qexec = QueryExecutionFactory.create(fieldsQquery, model)) {
            ResultSet results = qexec.execSelect();
            while (results.hasNext()) {
                QuerySolution solution = results.nextSolution();
                this.searchFields.add(solution.get("field"));
            }
        }

        // extract op-fts:return parameter
        Query returnVariableQuery = QueryFactory.create(this.getReturnVariable);
        try (QueryExecution qexec = QueryExecutionFactory.create(returnVariableQuery, model)) {
            ResultSet results = qexec.execSelect();
            while (results.hasNext()) {
                QuerySolution solution = results.nextSolution();
                // TODO ensure there's only one return variable
                this.returnVariable = solution.get("returnVariable");
            }
        }

        /*
            TODO extract other search parameters from BPG (see https://citnet.tech.ec.europa.eu/CITnet/confluence/display/ELISEARCH/v2+-+Federation+Service)
            Concretely: compute this.queryObjects
         */
    }
}