package com.example.springboot;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.apache.jena.sparql.algebra.*;

@RestController
public class FederationServiceController {
	HttpHeaders httpHeaders= new HttpHeaders();
	private static final String sparqlResponseXml = "<?xml version=\"1.0\"?>" +
			"<sparql xmlns=\"http://www.w3.org/2005/sparql-results#\">" +
			" <head>" +
			"   <variable name=\"exp\"/>" +
			" </head>" +
			" <results>" +
			"   <result>" +
			"     <binding name=\"exp\"><uri>http://data.legilux.public.lu/eli/etat/leg/rgd/2022/03/09/a97/jo</uri></binding>" +
			"   </result>" +
			" </results>" +
			"</sparql>";
	private static final String sparqlResponseJson = "{" +
  		"\"head\": { \"vars\": [ \"exp\" ]" +
		"} ," +
		"	\"results\": {" +
		"\"bindings\": [" +
		"{" +
		"	\"exp\": { \"type\": \"uri\" , \"value\": \"http://data.legilux.public.lu/eli/etat/leg/rgd/2022/03/09/a97/jo\" }" +
		"}" +
    		"]"+
	"}"+
	"}";
	@PostMapping("/sparql")
	public ResponseEntity processRequest(@RequestParam("query") String queryStr, @RequestHeader("Accept") String accept) {
		// create query object from query string
		Query queryObj = QueryFactory.create(queryStr);
		// compute corresponding SPARQL algebra
		Op algebra = Algebra.compile(queryObj);
		//create visitor object and traverse algebra
		FederatedQueryAlgebraVisitor visitor = new FederatedQueryAlgebraVisitor();
		OpWalker.walk(algebra, visitor);

		/* TODO next steps
			- build OpenSearch query
			- execute OpenSearch query
			- generate SPARQL response from OpenSearch response
			- return SPARQL response
		 */

		// return static SPARQL result depending on http accept header
		HttpHeaders responseHeaders = new HttpHeaders();
		String sparqlResponse = "";
		if (accept.equals("application/sparql-results+json")) {
			responseHeaders.add("Content-Type", MediaType.APPLICATION_JSON.toString());
			sparqlResponse = sparqlResponseJson;
		} else {
			responseHeaders.add("Content-Type", MediaType.APPLICATION_XML.toString());
			sparqlResponse = sparqlResponseXml;
		}
		return new ResponseEntity<>(sparqlResponse, responseHeaders, HttpStatus.OK);
	}
}
